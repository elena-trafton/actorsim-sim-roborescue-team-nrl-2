package nrl.actorsim.roborescue;

import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

import static nrl.actorsim.roborescue.CentralizedDispatcher.NULL_ENTITY_ID;

public class RRWorldObject extends WorldObject {
    public static RRWorldObject NULL_RR_WORLD_OBJECT = new RRWorldObject();

    private final StandardEntityURN urn;
    private EntityID entityId = NULL_ENTITY_ID;
    EntityID location = NULL_ENTITY_ID;

    protected RRWorldObject() {
        super(WorldType.NULL_TYPE);
        urn = StandardEntityURN.WORLD;
    }

    public RRWorldObject(WorldType type) {
        super(type);
        urn = StandardEntityURN.WORLD;
    }

    public RRWorldObject(StandardEntityURN urn) {
        super(urn.name());
        this.urn = urn;
    }

    public RRWorldObject(StandardEntityURN urn, EntityID entityId) {
        super(urn.name());
        this.urn = urn;
        this.setEntityId(entityId);
    }


    // ====================================================
    // Type Interface
    // ====================================================

    public RRWorldObject instance(EntityID id) {
        return new RRWorldObject(this.getUrn(), id);
    }

    // ====================================================
    // Object Clones
    // ====================================================


    @Override
    public RRWorldObject instance() {
        RRWorldObject instance = new RRWorldObject(this.getUrn(), this.getEntityId());
        instance.location = this.location;
        return instance;
    }


    // ====================================================
    // Other methods
    // ====================================================

    public StandardEntityURN getUrn() {
        return urn;
    }

    public EntityID getEntityId() {
        return entityId;
    }

    public void setEntityId(EntityID entityId) {
        if (entityId != NULL_ENTITY_ID) {
            setId(Integer.toString(entityId.getValue()));
        }
        this.entityId = entityId;
    }

    @Override
    public void update(WorldObject other) {
        super.update(other);
        if (other instanceof RRWorldObject) {
            RRWorldObject otherRRWorldObject = (RRWorldObject) other;
            this.location = ((RRWorldObject) other).location;
        }
    }

    public void substitute(EntityID id) {
        this.substitute(id.toString());
        this.setEntityId(id);
    }

}
