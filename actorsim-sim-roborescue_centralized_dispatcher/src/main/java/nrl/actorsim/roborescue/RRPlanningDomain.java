package nrl.actorsim.roborescue;

import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.domain.StatePredicate;
import nrl.actorsim.domain.WorldType;
import rescuecore2.standard.entities.StandardEntityURN;

import java.util.Arrays;

public class RRPlanningDomain extends PlanningDomain {
    public static final RRHumanObject CIVILIAN = new RRHumanObject(StandardEntityURN.CIVILIAN);
    public static final RRHumanObject AMBULANCE = new RRHumanObject(StandardEntityURN.AMBULANCE_TEAM);
    public static final RRHumanObject BRIGADE = new RRHumanObject(StandardEntityURN.FIRE_BRIGADE);
    public static final RRHumanObject POLICE = new RRHumanObject(StandardEntityURN.POLICE_FORCE);

    public static final WorldType HUMAN = new WorldType("human");
    public static final RRWorldObject BLOCKADES = new RRWorldObject(StandardEntityURN.BLOCKADE);

    static {
        for (RRHumanObject obj : Arrays.asList(CIVILIAN, AMBULANCE, BRIGADE, POLICE)) {
            obj.addSuperType(HUMAN);
        }
    }

    public static final WorldType AGENT = new WorldType("agent");
    static {
        for (RRHumanObject obj : Arrays.asList(AMBULANCE, BRIGADE, POLICE)) {
            obj.addSuperType(AGENT);
        }
    }

    public static StatePredicate BURIED = StatePredicate.builder().name("buried")
            .argTypes(HUMAN).build();

    public static StatePredicate NEEDS_RESCUE = StatePredicate.builder()
            .name("needs_rescue").argTypes(HUMAN).build();
    public static StatePredicate TRANSPORTING = StatePredicate.builder()
            .name("transporting").argTypes(AMBULANCE, CIVILIAN).build();

    public static StatePredicate ASSIGNED = StatePredicate.builder().name("assigned").argTypes(AGENT).build();

    public static RRWorldObject BUILDING = new RRWorldObject(StandardEntityURN.BUILDING);
    public static StatePredicate DESTROYED = StatePredicate.builder()
            .name("destroyed").argTypes(BUILDING).build();
    public static StatePredicate EMPTY_TANK = StatePredicate.builder()
            .name("empty_tank").argTypes(BRIGADE).build();
    public static StatePredicate ON_FIRE = StatePredicate.builder()
            .name("on_fire").argTypes(BUILDING).build();
    public static StatePredicate EXPLORED_BUILDING = StatePredicate.builder()
            .name("explored").argTypes(BUILDING).build();

    public static RRWorldObject ROAD = new RRWorldObject(StandardEntityURN.ROAD);
    public static StatePredicate EXPLORED_ROAD = StatePredicate.builder()
            .name("explored").argTypes(ROAD).build();

    public static StatePredicate BLOCKED_WITH_AGENT = StatePredicate.builder()
            .name("stuck_agents_at").argTypes(ROAD).build();
    public static StatePredicate BLOCKED_WITH_CIVILIAN = StatePredicate.builder()
            .name("stuck_civilians_at").argTypes(ROAD).build();
    public static StatePredicate CONTAINS_BLOCKADE = StatePredicate.builder()
            .name("blockades_in").argTypes(ROAD).build();

    public RRPlanningDomain(PlanningDomain.Options options) {
        super(options);
    }
}
