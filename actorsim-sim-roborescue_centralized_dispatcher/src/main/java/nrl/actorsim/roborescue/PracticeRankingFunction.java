package nrl.actorsim.roborescue;

import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.goalrefinement.strategies.ApplyStrategyDetails;
import nrl.actorsim.cognitive.RankingFunction;

public class PracticeRankingFunction implements RankingFunction {

    @Override
    public void updateAtStartOfCognitiveCycle(WorkingMemory memory) {

    }

    @Override
    public void updateDuringEachCognitiveCycleIteration(WorkingMemory memory) {

    }

    @Override
    public int compare(ApplyStrategyDetails o1, ApplyStrategyDetails o2) {
        return 0;
    }
}
