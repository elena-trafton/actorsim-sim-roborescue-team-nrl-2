package nrl.actorsim.roborescue;

import nrl.actorsim.domain.WorldType;
import nrl.actorsim.domain.WorldObject;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

public class RRHumanObject extends RRWorldObject {
    public static final RRHumanObject PLACEHOLDER = new RRHumanObject();

    protected int buriedness = 0;
    protected int damage = 0;

    protected RRHumanObject() {
        super();
    }

    public RRHumanObject(WorldType type) {
        super(type);
    }

    public RRHumanObject(StandardEntityURN urn) {
        super(urn);
    }

    public RRHumanObject(StandardEntityURN urn, EntityID id) {
        super(urn, id);
    }

    @Override
    public RRHumanObject instance() {
        RRHumanObject instance = new RRHumanObject(this.getUrn(), this.getEntityId());
        instance.location = this.location;
        instance.buriedness = this.buriedness;
        instance.damage = this.damage;
        return instance;
    }

    @Override
    public void update(WorldObject other) {
        if (other instanceof RRWorldObject) {
            super.update(other);
        }
        if (other instanceof RRHumanObject) {
            RRHumanObject otherRRWorldObject = (RRHumanObject) other;
            this.buriedness = otherRRWorldObject.buriedness;
            this.damage = otherRRWorldObject.damage;
        }
    }

    public RRHumanObject getInstance(EntityID id) {
        return new RRHumanObject(this.getUrn(), id);
    }

    public void substitute(EntityID id) {
        this.substitute(id.toString());
        this.setEntityId(id);
    }

    public void setBuriedness(int buriedness) {
        this.buriedness = buriedness;
    }

    public int getBuriedness() {
        return buriedness;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
