package nrl.actorsim.roborescue;

import adf.component.AbstractLoader;
import adf.component.tactics.*;
import adf.sample.tactics.SampleTacticsFireStation;
import adf.sample.tactics.SampleTacticsPoliceOffice;

public class NRLCentralizedDispatcherLoader extends AbstractLoader{
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLCentralizedDispatcherLoader.class);

	@Override
	public String getTeamName() {
		return "NRL";
	}

	@Override
    public TacticsAmbulanceTeam getTacticsAmbulanceTeam() { return new NRLAmbulanceTactics(); }

    @Override
    public TacticsFireBrigade getTacticsFireBrigade() {
        return new NRLFireBrigadeTactics();
    }

    @Override
    public TacticsPoliceForce getTacticsPoliceForce() {
        return new NRLPoliceTactics();
    }

    @Override
    public TacticsAmbulanceCentre getTacticsAmbulanceCentre() { return new CentralizedPassthroughAmbulanceCenter(); }

    @Override
    public TacticsFireStation getTacticsFireStation() {
        return new SampleTacticsFireStation();
    }

    @Override
    public TacticsPoliceOffice getTacticsPoliceOffice() {
        return new SampleTacticsPoliceOffice();
    }

}
