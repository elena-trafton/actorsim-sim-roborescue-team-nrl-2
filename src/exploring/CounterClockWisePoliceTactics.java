package exploring;

import java.util.List;
import java.util.Set;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.MessageUtil;
import adf.agent.communication.standard.bundle.StandardMessage;
import adf.agent.communication.standard.bundle.information.MessageBuilding;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.communication.CommunicationMessage;
import adf.component.extaction.ExtAction;
import adf.component.tactics.TacticsPoliceForce;
import adf.debug.WorldViewLauncher;
import rescuecore2.standard.entities.*;
import rescuecore2.worldmodel.EntityID;

public class CounterClockWisePoliceTactics extends TacticsPoliceForce{

	private Boolean isVisualDebug;
	private Graph graph;
	private List<EntityID> previousPath;
	private ExtAction actionExtClear;
	

	@Override
	public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {

		worldInfo.indexClass(
				StandardEntityURN.ROAD,
				StandardEntityURN.HYDRANT,
				StandardEntityURN.BUILDING,
				StandardEntityURN.REFUGE,
				StandardEntityURN.BLOCKADE
				);

		this.isVisualDebug = (scenarioInfo.isDebugMode()
				&& moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));
		this.graph = new Graph(worldInfo.getRawWorld());
		this.previousPath = null;
		this.actionExtClear = new ClearPathExtAction(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
	}

	@Override
	public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
		modulesPrecompute(precomputeData);		
	}

	@Override
	public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
			PrecomputeData precomputeData, DevelopData developData) {
		modulesResume(precomputeData);
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}

	}

	@Override
	public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, DevelopData developData) {
		modulesPreparate();
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}

	}

	@Override
	public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
		
		
		this.updateWorldInfoFromMessages(agentInfo, worldInfo, messageManager);
		this.sendUpdates(agentInfo, worldInfo, messageManager);
		
		PoliceForce agent = (PoliceForce) agentInfo.me();
		StandardEntity area = worldInfo.getEntity(agent.getPosition());
		boolean normalMove = false;
		Action action = null;
		if(area instanceof Area
				&& ((Area) area).getBlockades() != null
				&& ((Area) area).getBlockades().size() > 0) {
			action = this.actionExtClear.setTarget(agent.getPosition()).calc().getAction();

		}
		else {
			if(previousPath != null) {
				List<EntityID> newPath;
				if(previousPath.indexOf(agent.getPosition()) == -1) {
					newPath = previousPath;
					newPath.add(0, agent.getPosition());
				}
				else{
					newPath = previousPath.subList(previousPath.indexOf(agent.getPosition()), previousPath.size());
				}
				if(newPath.size() > 1) {
					StandardEntity next = worldInfo.getEntity(newPath.get(1));
					if(next instanceof Area
							&& ((Area) next).getBlockades() != null
							&& ((Area) next).getBlockades().size() > 0) {
						action = this.actionExtClear.setTarget(next.getID()).calc().getAction();
						
					}
				}
				if(action == null) {
					normalMove = true;
					action = new ActionMove(newPath);
				}
				previousPath = newPath;
			}
			if(previousPath == null 
					|| (normalMove 
							&& ((ActionMove)action).getPath().size() <= 1)) {
				List<EntityID> cycle = graph.findCycle(agent.getPosition());
				if(cycle.size() > 1) {
					action = new ActionMove(cycle);
					previousPath = cycle;
				}
				else {
					action = new ActionRest();
				}
			}
		}
		
		return action;
	}
	
	private void updateWorldInfoFromMessages(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		changedEntitiesIDs.add(agentInfo.getID());
		for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
			if (message instanceof StandardMessage) {
				MessageUtil.reflectMessage(worldInfo, (StandardMessage) message);
			}
		}

	}
	
	private void sendUpdates(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		for(EntityID changedEntityID : changedEntitiesIDs) {
			StandardEntity entity = worldInfo.getEntity(changedEntityID);
			if(entity.getStandardURN() == StandardEntityURN.BUILDING
					&& StaticUtilityMethods.isOnFireOrWaterDameged((Building)entity)){
				messageManager.addMessage(new MessageBuilding(true, (Building)entity));
			}
		}
	}



}
