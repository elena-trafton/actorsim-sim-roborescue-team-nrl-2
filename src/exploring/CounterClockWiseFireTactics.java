package exploring;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.MessageUtil;
import adf.agent.communication.standard.bundle.information.MessageBuilding;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.communication.CommunicationMessage;
import adf.component.extaction.ExtAction;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsFireBrigade;
import adf.debug.WorldViewLauncher;
import adf.sample.module.algorithm.SamplePathPlanning;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.FireBrigade;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

public class CounterClockWiseFireTactics extends TacticsFireBrigade{

	private Boolean isVisualDebug;
	private Graph graph;
	private List<EntityID> previousPath;
	private ExtAction actionExtFireFight;
	private PathPlanning pathPlanning;

	@Override
	public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {

		worldInfo.indexClass(
				StandardEntityURN.ROAD,
				StandardEntityURN.HYDRANT,
				StandardEntityURN.BUILDING,
				StandardEntityURN.REFUGE,
				StandardEntityURN.GAS_STATION,
				StandardEntityURN.AMBULANCE_CENTRE,
				StandardEntityURN.FIRE_STATION,
				StandardEntityURN.POLICE_OFFICE
				);

		this.isVisualDebug = (scenarioInfo.isDebugMode()
				&& moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));
		this.graph = new Graph(worldInfo.getRawWorld());
		this.previousPath = null;
		this.actionExtFireFight = new FireFightExtAction(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
		this.pathPlanning = new SamplePathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
		
	}

	@Override
	public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
		modulesPrecompute(precomputeData);
	}

	@Override
	public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
			PrecomputeData precomputeData, DevelopData developData) {
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}
	}

	@Override
	public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, DevelopData developData) {
		modulesPreparate();
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}
	}

	@Override
	public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {

		updateWorldInfoFromMessages(agentInfo, worldInfo, messageManager);

		this.modulesUpdateInfo(messageManager);
		this.sendUpdates(agentInfo, worldInfo, messageManager);

		FireBrigade agent = (FireBrigade)agentInfo.me();
		EntityID position = agent.getPosition();
		Action action = null;
		int indexOfPos = previousPath == null ? 0 : previousPath.indexOf(position);
		if(previousPath == null || indexOfPos == previousPath.size() - 1  || previousPath.size() == 0) {
			List<EntityID> cycle = graph.findCycle(position);
			if(cycle.size() <= 1) {
				cycle.add(getFirstNeighborRoad(position, worldInfo));
			}
			previousPath = cycle;
			indexOfPos = 0;
		}
		EntityID target;
		List<EntityID> currentPath;
		EntityID hottestBuilding = findHottestBuilding(worldInfo);
		if(hottestBuilding != null) {
			target = this.getFirstNeighborRoad(hottestBuilding, worldInfo);
			currentPath = previousPath;
		}
		else if(indexOfPos == -1) {
			pathPlanning.setFrom(position);
			if(previousPath.size() != 0) {
				pathPlanning.setDestination(previousPath.get(0));
			}
			else {
				pathPlanning.setDestination(getFirstNeighborRoad(position, worldInfo));
			}
			List<EntityID> returnPath = pathPlanning.calc().getResult();
			target = returnPath.get(0);
			currentPath = returnPath;
		}
		else {
			previousPath = previousPath.subList(indexOfPos + 1, previousPath.size());
			if(previousPath.size() != 0) {
				target = previousPath.get(0);
			}
			else {
				target = getFirstNeighborRoad(position, worldInfo);
			}

			currentPath = previousPath;
		}

		action = actionExtFireFight.setTarget(target).calc().getAction();
		if(action == null) {			
			action = new ActionMove(currentPath);
		}
		return action;
	}

	private void updateWorldInfoFromMessages(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		changedEntitiesIDs.add(agentInfo.getID());
		for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
			if (message instanceof MessageBuilding) {
				MessageUtil.reflectMessage(worldInfo, (MessageBuilding) message);
			}
		}

	}

	private void sendUpdates(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		for(EntityID changedEntityID : changedEntitiesIDs) {
			StandardEntity entity = worldInfo.getEntity(changedEntityID);
			if(entity.getStandardURN() == StandardEntityURN.BUILDING
					&& StaticUtilityMethods.isOnFireOrWaterDameged((Building)entity)){
				messageManager.addMessage(new MessageBuilding(true, (Building)entity));
			}
		}
	}

	private EntityID getFirstNeighborRoad(EntityID currentPosition, WorldInfo worldInfo) {
		Set<EntityID> neighbors = graph.getNeighbors(currentPosition);
		for(EntityID neighbor : neighbors) {
			if(worldInfo.getEntity(neighbor) instanceof Road) {
				return neighbor;
			}
		}
		return null;
	}
	
	private EntityID findHottestBuilding(WorldInfo worldInfo) {
		Collection<Building> fireBuildings = worldInfo.getFireBuildings();
		Building hottestBuilding = null;
		int hottestTemperature = 0;
		for(Building building : fireBuildings) {	
			if(building.getTemperature() > hottestTemperature) {
				hottestTemperature = building.getTemperature();
				hottestBuilding = building;
			}
		}
		return hottestBuilding != null ? hottestBuilding.getID() : null;
	}

}
