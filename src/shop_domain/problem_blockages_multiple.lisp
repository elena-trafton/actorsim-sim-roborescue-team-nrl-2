(defproblem problem roborescue 
  (

   (type_ambulance ambulance_1)
   (available ambulance_1)
   (stuck ambulance_1)

   (type_ambulance ambulance_2)
   (available ambulance_2)

   (type_civilian civilian_1)
   (buried civilian_1)
   (injured civilian_1)
   (stuck civilian_1)

   (type_civilian civilian_2)
   (injured civilian_2)
   (stuck civilian_2)

   (type_road road_segment_1)
   (blocked road_segment_1)

   (type_area area_1)
   (blocked area_1)
   

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_shelter)
   (type_shelter placeholder_shelter)
   
   (placeholder placeholder_civilian)
   (type_civilian placeholder_civilian)
   
   (placeholder placeholder_police)
   (type_police placeholder_police)
   (available placeholder_police)
   
   )
  ((clear_blockages))
)
