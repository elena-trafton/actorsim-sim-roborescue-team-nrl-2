
(defdomain roborescue
  (

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; ------- Methods -------
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (:method (transport_hurt_civilians)
	    at_least_one_civilian_needs_processing
	    ;; pre
	    (and (type_civilian ?civilian)
		 (not (placeholder ?civilian))
		 (not (processed ?civilian)))
	    ;; subtasks
	    ((transport_hurt_civilian ?civilian)
	     (transport_hurt_civilians))

	    base_case
	    nil
	    nil
	    )

   (:method (transport_hurt_civilian ?civilian)
	    civilian_is_buried
	    ;; pre
	    (and (type_civilian ?civilian)
		 (buried ?civilian)
		 (not (processed ?civilian))
		 (not (placeholder ?civilian))
		 (type_ambulance ?ambulance)
		 (available ?ambulance) )
	    ;; subtasks
	    ((!assign_ambulance ?civilian ?ambulance)
	     (!move_to ?ambulance ?civilian)
	     (!unbury ?ambulance ?civilian)
	     (transport ?ambulance ?civilian)
	     (!unassign ?ambulance)
	     )

	    civilian_is_injured
	    ;; pre
	    (and (type_civilian ?civilian)
		 (injured ?civilian)
		 (not (processed ?civilian))
		 (not (placeholder ?civilian))
		 (type_ambulance ?ambulance)
		 (available ?ambulance) )
	    ;; subtasks
	    ((!assign_ambulance ?civilian ?ambulance)
	     (!move_to ?ambulance ?civilian)
	     (transport ?ambulance ?civilian)
	     (!unassign ?ambulance)
	     ) 
            )

   (:method (transport ?ambulance ?civilian)
	    transport_civilian_to_shelter
	    ;; prec
	    (and (type_ambulance ?ambulance)
	    	 (type_civilian ?civilian)
		 (type_shelter ?shelter) )
	    ;; subtasks
	    ((!assign_shelter ?ambulance ?civilian ?shelter)
	     (!load ?ambulance ?civilian)
	     (!move_to ?ambulance ?shelter)
	     (!unload ?ambulance ?civilian)
	     )
	    )

   (:method (douse_fires)
	    at_least_one_building_needs_processing
	    ;; pre
	    (and (type_building ?building)
		 (not (placeholder ?building))
		 (not (processed ?building)))
	    ;; subtasks
	    ((douse_fire ?building)
	     (douse_fires))

	    base_case
	    nil
	    nil
	    )

    (:method (douse_fire ?building)
	     ;; pre
	     (and (type_building ?building)
		  (burning ?building)
		  (not (processed ?building))
		  (type_brigade ?brigade)
		  (available ?brigade))
	     ;; subtasks
	     ((!assign_brigade ?building ?brigade)
	      (!refill_if_needed ?brigade)
	      (!move_near ?brigade ?building)
	      (!douse ?brigade ?building)
	      (!unassign ?brigade)
	      )
	     )

    (:method (clear_blockages)
	     ;;pre
	     nil
	     ;;subtasks
	     ((clear_agents)
	      (clear_civilians)
	      (clear_roads)
	      (clear_blockades)
	      )
	     )
    
    (:method (clear_agents)
	     stuck_agents
	     ;; pre
    	     (and (stuck ?agent) 
    		  (not (processed ?agent))
		  (not (type_civilian ?agent)))
	     ;; subtasks
	     ((clear_target ?agent)
	      (clear_agents))
	     
	     base_case
	     nil
	     nil
	     )
    
	     
    (:method (clear_civilians)
	     ;; pre
    	     (and (stuck ?civilian) 
		  (type_civilian ?civilian)
    		  (not (processed ?civilian)))
	     ;; subtasks
	     ((clear_target ?civilian)
	      (clear_civilians))

	     base_case
	     nil
	     nil
	     )

    (:method (clear_roads)
	     ;; pre
	     (and (type_road ?road_segment)
		  (blocked ?road_segment)
		  (not (processed ?road_segment)))
	     ;; subtasks
	     ((clear_target ?road_segment)
	      (clear_roads))

	     base_case
	     nil
	     nil
	     )


    (:method (clear_blockades)
	     ;; pre
	     (and (type_area ?area)
		  (blocked ?area)
		  (not (processed ?area)))
	     ;; subtasks
	     ((clear_target ?area)
	      (clear_blockades))

	     base_case
	     nil
	     nil
	     )

    (:method (clear_target ?target)
	     ;; pre
    	     (and (not (processed ?target))
		  (type_police ?police)
		  (available ?police))
	     ;; subtasks
	     ((!assign_police ?target ?police)
	      (!clear_road_to ?police ?target)
	      (!clear ?police ?target)
	      (!unassign ?police))
	     )

    ;; dubug for printing
    (:method (print-current-state)
	     nil nil nil)

    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; ------- Operators -------
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;example operator
    ;; (:operator (!collect_from_location ?item ?location)
    ;; 	       ;; prec
    ;; 	       (and (entity_at ?item ?location)
    ;; 		    (type_location ?location)
    ;; 		    )
    ;; 	       ;; delete the entity at predicate!
    ;; 	       ((entity_at ?item ?location))
    ;; 	       ;; add
    ;; 	       nil)

    (:operator (!move_to ?agent ?location)
	       nil nil nil)

    (:operator (!move_near ?agent ?location)
	       nil nil nil)

    ;; command executive to have agent move around while monitoring for item
    (:operator (!explore_for ?agent ?item)
	       nil nil nil)

    ;; unassigns the agent
    (:operator (!unassign ?agent)
	       ;; prec
	       (and (not (available ?agent)))
	       ;; delete
	       nil
	       ;; add
	       ((available ?agent)) )

    ;;
    ;; Ambulance Operators
    ;; 

    ;; assign ambulance; may be swapped at execution time
    (:operator (!assign_ambulance ?civilian ?ambulance)
	       ;; prec
	       (and (type_ambulance ?ambulance)
		    (available ?ambulance))
	       ;; delete
	       ((available ?ambulance))
	       ;; add
	       ((processed ?civilian)) )

    ;; assign a shelter; may be swapped at execution time
    (:operator (!assign_shelter ?agent ?civilian ?shelter)
	       ;; prec
	       (and (type_shelter ?shelter))
	       ;; delete
	       nil
	       ;; add assigned
	       nil)

    (:operator (!unbury ?ambulance ?civilian)
	       nil nil nil)

    (:operator (!load ?ambulance ?civilian)
	       nil nil nil)

    (:operator (!unload ?ambulance ?civilian)
	       nil nil nil)

    ;; 
    ;; Brigade Operators
    ;; 

    ;; assign brigade; may be swapped at execution time
    (:operator (!assign_brigade ?building ?brigade)
	       ;; prec
	       (and (type_brigade ?brigade)
		    (available ?brigade))
	       ;; delete
	       ((available ?brigade))
	       ;; add
	       ((processed ?building)))

    (:operator (!refill_if_needed ?brigade)
	       nil nil nil)
    
    (:operator (!douse ?brigade ?building)
	       nil nil nil)


    ;; 
    ;; Police Operators
    ;; 

    ;; assign police; may be swapped at execution time
    (:operator (!assign_police ?target ?brigade)
	       ;; prec
	       (and (type_police ?brigade)
		    (available ?brigade))
	       ;; delete
	       ((available ?brigade))
	       ;; add
	       ((processed ?target)))

    (:operator (!clear_road_to ?police ?target)
	       nil nil nil)

    (:operator (!clear ?police ?target)
	       nil nil nil)


    ;; 
    ;; Helper Operators
    ;; 

    ;; debug fail action to show that no preconditions matched in methods
    (:operator (!!fail)
	       nil nil nil)

    ;; debug fail action to show that no preconditions matched in methods
    (:operator (!!fail_with_message ?message)
	       nil nil nil)

    ;; debug success action to show that preconditions matched in methods
    (:operator (!!success)
	       nil nil nil)
    
    ;; debug success action to show that preconditions matched in methods
    (:operator (!!success_with_message ?message)
	       nil nil nil)
    
    ;; debug success action to show that the base case completed for recursive functions
    (:operator (!!completed_for ?object)
	       nil nil nil)

    ))



